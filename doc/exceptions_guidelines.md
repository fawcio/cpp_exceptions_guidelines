# C++ Exceptions Guide

|Author|Version|
|------|------|
|Slawomir Cielepak| 0.1|

**WARNING: Here be errors!**

This document is not an exhaustive and definitive guide to C++ Exceptions. The overall topic is very wide and can't be explained in a short paper.
This is a list of guidelines to help write C++ code that is:

* easier to read/understand
* easier to write
* more robust
* has no additional time penalty

---

### 1. Important assumptions

##### 1.1 C++ exceptions are the preferable way of reporting errors

##### 1.2 All code throws unless it is explicitly stated and guaranteed otherwise

* And we're okay with this!
* We don't catch every single exception possible everywhere


### 2. Goals

##### 2.1 Enforced error handling

##### 2.2 Exceptions safety:

* basic guarantee is **priority zero**
* strong guarantee is provided when it's "natural"
* no-throw guarantee is provided only when required

##### 2.3 Clean code


### 3. Guidelines

##### 3.0 Naming

Append 'Exception' to all custom exception class names you implement.

```c++
class CustomException : public std::exception
{
    ...
}
```

##### 3.1 Favor exceptions over error codes

Consider the following coding style:

```c++
result_t tryToDoSthUseful(void* arg, size_t size)
{
    result_t result = RESULT_OK;
    if (arg == nullptr)
    {
        result = RESULT_NULLPTR_ERROR;
    }
    else if (size = 0 || size > 1000500100900L)
    {
        result = RESULT_INCORRECT_SIZE;
    }
    else
    {
        // Do some work for start
        result = doFirstThing(arg);
        if (result == RESULT_OK)
        {
            // If it was successful, continue
            result = doSomeOtherWork(size);
            if (result == RESULT_OK)
            {
                // Again, continue if successful
                result = doSomeMoreWork();
                if (result == RESULT_OK)
                {
                    // And Finally
                    result = thisBecomeObscure();
                }
            }
        }
    }
  
    if (result != RESULT_OK)
    {
        cleanup();
    }
  
    return result;
}
```

* Is it readable?
* Is it robust?
* Is it easy for modification?

Now compare it to this example:

```c++
void maybeThisIsBetter(void* arg, size_t size)
{
    // Do some stuff
    doFirstThing(arg);
  
    // Continue with your work
    doSomeOtherWork(size);
  
    // Just do your business
    doSomeMoreWork();
  
    // And Finally
    goAndHaveSomeFun();
}
```

* This looks clean, but it still can fail.
* Will it force us to handle errors? If it throws, then yes, it will.
* Where is the cleanup in case of error? The answer is in the guideline: Use RAII.
* If it throws, where should we handle the exceptions? See next guidelines.

Conclusion: *Don't mix error handling wth business logic.*

##### 3.2 Use RAII

Please refer to section **7. RAII** in this document.

##### 3.3 Throw by value and catch by reference

```c++
try
{
    throw SomeDefinedException("Description of a exception"); // Derived from std::exception
}
catch (std::exception &e)
// SomeDefinedException will be caught thanks to polymorphic catch.
// Yes, there will be slicing.
{
    std::cerr << "Exception: " << e.what() << std::endl;
}
```

##### 3.4 When throwing an exception in constructor body, make sure no resources are left

* If you try/catch in constructor, remember to rethrow in case of error.

Example:
```c++
SomeClass::SomeClass()
{
    mIntBuffer = new int[1024];

    mObject = new CustomClass {}; // If this throws, mIntBuffer will leak!
}
```

We must take care of freeing resources in constructor body ourselves:
```c++
SomeClass::SomeClass()
{
    try
    {
        intBuffer = new int[1024];
        object = new CustomClass {};
    }
    catch (std::exception &e)
    {
        if (intBuffer) // Make sure intBuffer is initialized with nullptr
        {
            delete [] intBuffer;
        }
        if (object) // Make sure object is initialized with nullptr
        {
            delete object;
        }

        throw; // Rethrow the original error.
    }
}
```

##### 3.5 Make sure the exception class will never throw by itself

Example:
```c++
class ExceptionClass
{
 public:
     ExceptionClass() { throw std::runtime_error {"Exception from constructor"}; }
}
...
try
{
    throw ExceptionClass(); // This will throw by itself!
}
...
```

##### 3.6 Use public inheritance for exception classes only

Explanation: private/protected inherited classes will not be handled polymorphic-ally
```c++
class CustomException : private std::runtime_error
{
    ...
}

try
{
    throw CustomException {};
}
catch (std::runtime_error &e)
{
    // Nothing will be caught!
}
```

##### 3.7 Do not use dynamic exceptions specifications

* We assume that every function/method may throw if not said explicitly that it's not.
* This feature is deprecated since C++11
* Compiler doesn't provide any static checks whether method really conforms to exception specification

Example:
```c++
void someFunction() throw(std::runtime_error) // DON'T
{
    throw 123;
}

void willNotThrow() throw() // Also DON'T
{
    auto tab = new int[10e666];
}
```

##### 3.8 Derive exception classes at least from `std::exception`.

* Prefer deriving from `std::runtime_error`, `std::length_error`, `std::invalid_argument` etc.
* Remember to inherit publicly.

##### 3.9 Do use `noexcept` BUT, use it only for destructors, `std::swap()` and move operations, nowhere else.

In C++11 destructors are noexcept by default.

Example:
```c++
class ExampleClass
{
public:
    ...
    friend void swap(ExampleClass& first, ExampleClass& second) noexcept
    {
        using std::swap;
        swap(first.mSize, second.mSize);
        swap(first.mArray, second.mArray);
    }
    ...
};
```

##### 3.10 Don't throw from `noexcept` functions (especially: destructors, std::swap, and move operations)

   In practice we can throw, but never emit any exception outside! Catch everything within `noexcept` methods/functions.

Example:
```c++
class ExampleClass
{
public:
    ...
    friend void swap(ExampleClass& first, ExampleClass& second) noexcept
    {
        ...
        throw std::runtime_error {"This will call terminate! "}; // DON'T
    }
    ...
};
```

##### 3.11 Put try/catch in main()

* Catch not only std::exception&, but also all other unspecified exceptions.
* Catch nested exceptions

Example:
```c++
// recursively print exception whats:
void print_what(const std::exception &e)
{
    std::cerr << "(" << e.what() << ")\n";
    try
    {
        std::rethrow_if_nested(e);
    }
    catch (std::exception const &nested)
    {
        std::cerr << "nested: ";
        print_what(nested);
    }
    catch (...)
    {
        std::cerr << "nested: (unknown exception)\n";
    }
}

int main(void)
{
    try
    {
        runTheProgramLogic();
    }
    catch (std::exception const &e)
    {
        print_what(e);
    }
    catch (...)
    {
        std::cerr << "(unknown exception)\n";
    }
}
```

##### 3.12 Don't re-throw outside a catch() block

Example:
```c++
void Function()
{
    bool error = false;
    try
    {
        throw 123;
    }
    catch (std::exception &e)
    {
        handleException(e);
        error = true;
    }
    
    if (error)
        throw; // Rethrow exception <-- DON'T!
}
```

##### 3.13 Write placement delete if you write placement new.

Although this feature is rarely used, it has to be considered for exception safety.

If compiler doesn't find a matching placement delete, it will just use normal delete, possibly leaking resources.

Example:
```c++
class Widget
{
public:
    Widget() { throw std::runtime_error("Exception in destructor"); }

    // custom placement new
    static void* operator new(std::size_t sz, const std::string& msg)
    {
        std::cout << "custom placement new called, msg = " << msg << '\n';
        return ::operator new(sz);
    }

    // custom placement delete
    static void operator delete(void* ptr, const std::string& msg)
    {
        std::cout << "custom placement delete called, msg = " << msg << '\n';
        ::operator delete(ptr);
    }
};

int main()
{
    try
    {
        auto p1 = new ("widget") Widget;
    }
    catch(const std::exception& e)
    {
        std::cout << e.what() << '\n';
    }
}
```

##### 3.14 Provide a custom exceptions classes for different kind of errors

* We can use standard, meaningful C++ exceptions, but don't treat them specially. Just let the program fail.
* For specialized error handling, custom exception classes are essential.

Example:
```c++
class OperationNotCompleteException : public std::runtime_error {}
class SeriousDamageException : public std::runtime_error {}
...
try
{
    if (operationNotCompleted)
    {
        throw OperationNotCompleteException {"It's not that bad, really."};
    }
    else if (somethingReallyBadHappened)
    {
        throw SeriousDamageException {"Run for your lifes!"};
    }
}
catch (OperationNotCompleteException &e)
{
    // Just notify the user and continue with the job.
}
catch (SeriousDamageException &e)
{
    // &@$YTWRWRIU&#^bw^FGJ*()%@&$&YG
}
```

##### 3.15 Don't use `std::nothrow`

Using `std::nothrow` breaks the rule **3.1 Favor exceptions over error codes**. We do not want to handle errors by examining the return value, which `std::nothrow` enables.
It also disables throwing exceptions from `new`, so it breaks the error handling enforcement.

Example:
```c++
int* buffer = new (std::nothrow) int[10e666]; // Won't throw
if (buffer == nullptr)
{
    // Handle error?
}
```

---

### 4. When we should throw exceptions?

Shortly: In case of any error, that is not expected to happen "normally".

##### 4.1 Example 1 - error reporting

```c++
void someFunction(uint64_t longValue)
{
    if (longValue > 100500100900L)
    {
        throw std::invalid_argument {"Invalid parameter to someFunction()"};
    }
    ...
}
```

---

### 5. Where we musn't throw exceptions?

##### 5.1 Anywhere we support no-throw guarantee

* In destructors and cleanup operations
* `std::swap()`
* move operations

##### 5.2 In Threads

Throwing from a thread will terminate the program.

When running a thread always enclose its routine in try/catch just like in main() function or use std::async which passes the exception to calling thread.

Example:
```c++
int main()
{
    try
    {
        std::thread {[]() {
            throw std::runtime_error {"Exception from tread"}; // This will terminate!
        }};
    }
    catch(const std::exception& e) // Nothing will be caught
    {
        std::cout << e.what() << '\n';
    }
}
```

Instead:
```c++
int main()
{
    try
    {
        // std::async will catch the exception and rethrow it in the root thread
        auto result = std::async(std::launch::async, []() {
            throw std::runtime_error {"Exception from tread"};
        });
        result.get();
    }
    catch(const std::exception& e) // This will catch exception now.
    {
        std::cout << e.what() << '\n';
    }
}
```

For more sophisticated multithreading, a wrapping Thread class, that rethrows exceptions must be provided.

##### 5.3 To implement control flow

```c++
void isThisStringAcceptable(std::string const &s)
{
    if (s.compare("v0id") == 0)
    {
        throw false;
    }
    else
    {
        throw true;
    }
}
```


### 6. Where should we try/catch?

Often the best way to deal with exceptions is to not handle them at all. If you can let them pass through your code and allow destructors to handle cleanup, your code will be cleaner.

##### 6.1 Anywhere that we need to switch our method of error reporting.

If we need to return an error value (e.g. for C-API) or show an error to human by any kind of UI. Also we need to catch exceptions in threads either to move them (C++11) to the parent thread or to pass the error.

Example 1:
```c++
result_t adapterFunction(void) // Must return error code for compatibility with other API
{
    try
    {
        throw 123;
    }
    catch (...)
    {
        return RESULT_FAILED;
    }
    
    result RESULT_SUCCESS;
}
```

Example 2:
```c++
void refactorThisTrUnit(TrUnit &u)
{
    ...
    auto result = functionFromFramework(u);
    if (result != SUCCESS)
    {
        throw std::runtime_error("functionFromFramework failed");
    }
    ...
}
...
void applyRefactoringToProject(CppProject &p)
{
    try
    {
        for (auto unit : p.translationUnits() )
        {
            refactorThisTrUnit(unit);
        }
    }
    catch (std::exception &e)
    {
        showErrorToUser(e.what());
        return;
    }
}
```

##### 6.2 Anywhere we have a way of dealing with an error such as an alternative or fallback method.

If we don't, exceptions are simply not caught.

Example:
```c++
class Configuration
{
public:
    Configuration(std::string pathToConfigFile)
    {
        try
        {
            // Try to load a stored configuratiuon from file
            auto file = openConfigFile(pathToConfigFile);
            initConfigWithFile(file);
        }
        catch (ConfigFileMissingException &e)
        {
            // That's ok, we will init a default config
            initWithDefaultConfiguration();
        }
    }
}
```

##### 6.3 Anywhere that partial failure is acceptable.

If it's not, exceptions are simply not caught.

Example:
```c++
void refactorThisTrUnit(TrUnit &u)
{
    ...
    auto result = functionFromFramework(u);
    if (result != SUCCESS)
    {
        throw std::runtime_error("functionFromFramework failed");
    }
    ...
}
...
void applyRefactoringToProject(CppProject &p)
{
    for (auto unit : p.translationUnits())
    {
        try
        {
            refactorThisTrUnit(unit);
        }
        catch (CriticalRefactoringError &e)
        {
            callThePolice();
            showErrorToUser(e.what());
            return; // Stop processing.
        }
        catch (OtherMinorError &e)
        {
            showWarningToUser(e.what());
            // Continue with other units
        }
    }
}
```

---

### 7. RAII

Universal Example:
```c++
class WrapperClass
{
public:
    WrapperClass(size_t size)
    {
        buffer = new uint8_t[size];
    }
    ~WrapperClass()
    {
        if (buffer)
        {
            delete [] buffer;
        }
    }
private:
    uint8_t* buffer {nullptr};
}
```

##### 7.1 Wrap any resource that need to be taken responsibility for in a class.

Not only "newed" memory, but also any resource, like a handle to system specific entity (socket, file descriptor, a state).

##### 7.2 Use constructors for allocating resources and destructors for releasing them.

* Don't implement `init()` methods.
* Avoid implementing `cleanup()` methods. If you do, call them in a destructor.

##### 7.3 Don't use "naked" `new` outside constructors.

* Use `new` only in constructors.
* Always wrap an allocable resource into a class that will manage its lifetime or use C++11 smart pointers. Use `std::make_shared` and `std::make_unique` for smart pointers.

##### 7.4 Strive to manage one resource by one object.

See: Single responsibility principle.

##### 7.5 Don't write any cleanup code that isn't being called by a destructor.

If you really need a `cleanup()` function, simply call it in a destructor.

##### 7.6 Disable copy constructors by default.

Either by marking them with `delete` or by inheriting from a "NonCopyable" class.

##### 7.7 If you need object copy semantics, stick to "rule of four".

Please refer to the great article on stackoverflow.com: [What is the copy-and-swap idiom](http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom) - this implementation will give a strong exception guarantee.

One exception to this rule is that you don't need to provide `std::swap()` specialization if you provide no-except move operations. 

##### 7.8 Think twice before defining a copyable class! It's very likely you don't need it.